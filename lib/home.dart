import 'package:flutter/material.dart';
import 'package:test_animations/animations/transform_02_3d_perspective.dart';

import 'animations/metaballs.dart';
import 'animations/transform_01_rotation.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  static const List<Widget> animations = [
    MetaBalls(
      // ballColor: Colors.green,
      // ballColor: Colors.blueAccent,
      // ballColor: Colors.lightBlueAccent,
      ballColor: Colors.purple,

      // amount: 7,
      // radius: 300,
      // resolution: 10000,
      // curve: Cubic(1, 1, 0, 1),
      // velocity: 2,
      // amount: 7,
      //
      // amount: 30,
      // radius: 300,
      // resolution: 8000,
      // curve: Curves.easeInCirc,
      // velocity: 2,
      //
      amount: 10,
      radius: 300,
      resolution: 8000,
      velocity: 2,
    ),
    MetaBalls(),
    Transform01_Rotation(),
    Transform02_3DPerspective(),
  ];

  late TabController controller;
  late String title;

  void setTitle() => title = "${animations[controller.index].runtimeType}";

  @override
  void initState() {
    super.initState();
    controller = TabController(
      length: animations.length,
      vsync: this,
    )..addListener(() {
        setState(setTitle);
      });

    setTitle();
  }

  @override
  Widget build(BuildContext context) {
    final showBackArrow = controller.index > 0;
    final showForwardArrow = controller.index < animations.length - 1;

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: [
          if (showBackArrow)
            IconButton(
              splashColor: Colors.transparent,
              splashRadius: 20,
              onPressed: () {
                if (0 == controller.index) return;
                controller.animateTo(controller.index - 1);
              },
              icon: const Icon(Icons.arrow_back_rounded),
            ),
          Center(
            child: Text(
              controller.index.toString(),
              textScaleFactor: 1.6,
            ),
          ),
          Opacity(
            opacity: showForwardArrow ? 1 : 0,
            child: IgnorePointer(
              ignoring: !showForwardArrow,
              child: IconButton(
                splashColor: Colors.transparent,
                splashRadius: 20,
                onPressed: () {
                  if (animations.length - 1 == controller.index) return;
                  controller.animateTo(controller.index + 1);
                },
                icon: const Icon(Icons.arrow_forward_rounded),
              ),
            ),
          ),
        ],
      ),
      body: TabBarView(
        physics: const NeverScrollableScrollPhysics(),
        controller: controller,
        children: animations,
      ),
    );
  }
}
