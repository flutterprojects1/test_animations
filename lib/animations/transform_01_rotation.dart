// ignore_for_file: camel_case_types

import 'dart:math';

import 'package:flutter/material.dart';

/// [See](https://medium.com/flutter-community/a-deep-dive-into-transform-widgets-in-flutter-4dc32cd575a9)
class Transform01_Rotation extends StatefulWidget {
  const Transform01_Rotation({Key? key}) : super(key: key);

  @override
  _Transform01_RotationState createState() => _Transform01_RotationState();
}

class _Transform01_RotationState extends State<Transform01_Rotation> {
  double x = 0;
  double y = 0;
  double z = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Transform(
          transform: Matrix4(
            1, 0, 0, 0, //
            0, 1, 0, 0, //
            0, 0, 1, 0, //
            0, 0, 0, 1, //
          )
            ..rotateX(x)
            ..rotateY(y)
            ..rotateZ(z),
          alignment: FractionalOffset.center,
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                y = y - details.delta.dx / 100;
                x = x + details.delta.dy / 100;
              });
            },
            child: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  transform: GradientRotation(pi / 2),
                  colors: [
                    Colors.red,
                    Colors.green,
                  ],
                ),
              ),
              height: 200,
              width: 200,
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {
          x = 0;
          y = 0;
          z = 0;
        }),
        child: const Icon(Icons.refresh_outlined),
      ),
    );
  }
}
