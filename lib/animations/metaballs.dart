import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:funvas/funvas.dart';

/// Inspirations
///
/// * [Coding Train](https://www.youtube.com/watch?v=ccYLb7cLB1I)
/// * [SuperDeclarative!](https://www.youtube.com/watch?v=Iyh81vS3lFM&t=0s)
/// * [Metaballs 60FPS](https://www.youtube.com/watch?app=desktop&v=N8o4bC8GouY)
class MetaBalls extends StatelessWidget {
  const MetaBalls({
    Key? key,
    this.amount = 7,
    this.radius = 750,
    this.resolution = 8190,
    this.velocity = 5.0,
    this.curve = const Cubic(1, 1, 0, 1),
    this.backgroundColor = Colors.black,
    this.ballColor = Colors.white,
  }) : super(key: key);

  /// Amount of balls.
  final int amount;

  /// The velocity factor for each ball.
  final double velocity;

  /// The radius of each ball.
  final double radius;

  /// The resolution for the radial [ui.Gradient] shader.
  final int resolution;

  /// The [Curve] that is applied to every color stop.
  final Curve curve;

  final Color backgroundColor;

  /// The ball color is interpolated from 0x00 alpha to 0xFF alpha.
  final Color ballColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: backgroundColor,
      child: FunvasContainer(
        funvas: _MetaBallsFunvas(
          velocity: velocity,
          amount: amount,
          ballRadius: radius,
          resolution: resolution,
          curve: curve,
          color: ballColor,
        ),
      ),
    );
  }
}

class _MetaBallsFunvas extends Funvas {
  List<_MetaBall>? _balls;
  List<Color>? _gradientColors;
  List<double>? _gradientStops;

  final int amount;
  final double ballRadius;
  final int resolution;
  final Curve curve;
  final double velocity;
  final Color color;

  _MetaBallsFunvas({
    required this.amount,
    required this.ballRadius,
    required this.resolution,
    required this.velocity,
    required this.color,
    required this.curve,
  })  : assert(0 <= amount),
        assert(0 < ballRadius);

  @override
  void u(double t) {
    final size = Size(x.width, x.height);

    if (null == _balls) {
      final random = math.Random(DateTime.now().millisecondsSinceEpoch);
      _balls = List.generate(
        amount,
        (index) => _MetaBall(
          offset: Offset(
            ballRadius + random.nextDouble() * (size.width - 2 * ballRadius),
            ballRadius + random.nextDouble() * (size.height - 2 * ballRadius),
          ),
          velocity: Offset.fromDirection(
            random.nextDouble() * 2 * math.pi,
            velocity,
          ),
          screenSize: size,
          radius: ballRadius,
        ),
      );
    }

    if (null == _gradientColors && null == _gradientStops) {
      _gradientColors = [];
      _gradientStops = [];
      for (var i = 0; i <= resolution; i++) {
        final current = i / resolution;

        _gradientStops!.add(math.pow(current, 2).toDouble());
        _gradientColors!.add(
          Color.fromRGBO(
            color.red,
            color.green,
            color.blue,
            curve.transform(math.pow(1 - current, 2).toDouble()),
          ),
        );
      }
    }

    for (final ball in _balls!) {
      final shader = ui.Gradient.radial(
        ball.offset,
        ball.radius,
        _gradientColors!,
        _gradientStops!,
        TileMode.clamp,
      );

      c.drawCircle(
        ball.offset,
        ball.radius,
        Paint()
          ..shader = shader
          ..blendMode = BlendMode.plus,
      );

      ball.move();
    }
  }
}

class _MetaBall {
  final Size screenSize;
  final double radius;
  Offset offset;
  Offset velocity;

  _MetaBall({
    required this.offset,
    required this.screenSize,
    required this.radius,
    required this.velocity,
  });

  void setPosition(double x, double y) => offset = Offset(x, y);

  void move([bool checkRadius = false]) {
    final x = offset.dx;
    final y = offset.dy;
    final radius = checkRadius ? this.radius : 0;

    // Check right and left edge
    if (x + velocity.dx + radius > screenSize.width ||
        x + velocity.dx - radius < 0)
      velocity = velocity.overwrite(dx: -velocity.dx);

    // Check bottom and top edge
    if (y + velocity.dy + radius > screenSize.height ||
        y + velocity.dy - radius < 0)
      velocity = velocity.overwrite(dy: -velocity.dy);

    setPosition(x + velocity.dx, y + velocity.dy);
  }
}

extension OverwriteExtension on Offset {
  Offset overwrite({double? dx, double? dy}) =>
      Offset(dx ?? this.dx, dy ?? this.dy);
}
