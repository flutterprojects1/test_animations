// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

/// [See](https://medium.com/flutter/perspective-on-flutter-6f832f4d912e)
class Transform02_3DPerspective extends StatefulWidget {
  const Transform02_3DPerspective({Key? key}) : super(key: key); // changed

  @override
  _Transform02_3DPerspectiveState createState() =>
      _Transform02_3DPerspectiveState();
}

class _Transform02_3DPerspectiveState extends State<Transform02_3DPerspective> {
  static const rotationSensivity = 0.01;

  int counter = 0;
  Offset rotation = Offset.zero;

  void incrementCounter() {
    setState(() {
      counter++;
    });
  }

  Widget defaultApp(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: const Text("The Matrix 3D"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "You have pushed the button this many times:",
            ),
            Text(
              '$counter',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: incrementCounter,
        tooltip: "Increment",
        child: const Icon(Icons.add),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.identity()
        // perspective
        // ..setEntry(3, 2, 0.001)
        ..setEntry(3, 2, 0.005)
        // ..setEntry(3, 2, -0.005)
        //
        ..rotateX(rotationSensivity * rotation.dy)
        //
        // The parameter to rotateY is negated because
        // as the finger moves to the right, the image
        // rotates counter-clockwise around the Y axis
        //(because the Y axis is pointing downward).
        ..rotateY(-rotationSensivity * rotation.dx),
      alignment: FractionalOffset.center,
      child: GestureDetector(
        onPanUpdate: (details) => setState(() => rotation += details.delta),
        onDoubleTap: () => setState(() => rotation = Offset.zero),
        child: defaultApp(context),
      ),
    );
  }
}
