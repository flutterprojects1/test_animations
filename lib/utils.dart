import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';

/*-------------------------------------------------------------------------------------------*/
/*                                 Some pre-defined methods                                  */
/*-------------------------------------------------------------------------------------------*/

/// Loads all assets from the `assets/` directory.
Future<void> loadAllAssets([bool showLog = false]) async {
  // TODO: Add your own assets
  //
  // example:
  //
  // const String assets = "assets/";
  // loadImage(assets + "test.png");
}

/// percent has to be within [0;1]
double getWindowWidth(BuildContext context, double percent) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.width * percent;
}

/// percent has to be within [0;1]
double getWindowHeight(BuildContext context, double percent) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.height * percent;
}

Future<Uint8List> loadImage(String url) {
  final Completer<Uint8List> completer = Completer<Uint8List>();
  final ImageStream imageStream =
      AssetImage(url).resolve(ImageConfiguration.empty);

  late ImageStreamListener listener;
  listener = ImageStreamListener(
    (imageInfo, synchronousCall) {
      imageInfo.image.toByteData(format: ImageByteFormat.png).then((byteData) {
        imageStream.removeListener(listener);
        completer.complete(byteData!.buffer.asUint8List());
      });
    },
    onError: (exception, stackTrace) {
      imageStream.removeListener(listener);
      completer.completeError(exception);
    },
  );

  imageStream.addListener(listener);

  return completer.future;
}

extension SimpleClassName on Object {
  String? get simpleClassName => runtimeType.toString();
}

extension RandomElement<T> on List<T> {
  T getRandom([Random? random]) {
    random ??= Random();
    return this[random.nextInt(length)];
  }
}

